rpiboot: main.cpp
	$(CXX) -Wall -Wextra -g -D_FILE_OFFSET_BITS=64 -o $@ $< -lusb-1.0 -lboost_thread -lboost_system -lboost_chrono -lpthread

uninstall:
	rm -f /usr/bin/rpiboot
	rm -f /usr/share/rpiboot/bootcode.bin
	rm -f /usr/share/rpiboot/start.elf
	rm -f /usr/share/rpiboot/usbbootcode.bin
	rm -f /usr/share/rpiboot/msd.elf
	rm -f /usr/share/rpiboot/buildroot.elf
	rmdir --ignore-fail-on-non-empty /usr/share/rpiboot/

clean: 
	rm rpiboot

install: rpiboot
	mkdir -p /usr/share/rpiboot
	cp ./msd/bootcode.bin /usr/share/rpiboot
	cp ./msd/start.elf /usr/share/rpiboot
	cp ./rpiboot /usr/bin
	chmod a-r /usr/bin/rpiboot
	chown root /usr/bin/rpiboot
	chmod u+s /usr/bin/rpiboot
	

.PHONY: uninstall clean
