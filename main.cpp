#include <libusb-1.0/libusb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>

#include <unistd.h>

// For reading from /sys/block
#include <sys/types.h>
#include <sys/mount.h>
#include <dirent.h>

#include <set>
#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <utility>

#include <boost/algorithm/string.hpp>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>

using std::string;
using std::vector;
using std::ifstream;
using std::set;
using std::multimap;
using std::map;
using std::cout;
using std::endl;
using std::cin;
using std::ostringstream;
using std::pair;

boost::mutex output_lock;
boost::mutex bulk_lock;

int verbose = 0;
int loop = 0;
int bulk = 0;
int killed = 0;
int assumeY = 0;
const char * directory = NULL;
const char * image = NULL;

typedef struct MESSAGE_S {
		int length;
		unsigned char signature[20];
} boot_message_t;

void my_handler(int s){
	killed = 1;
	if(loop != 0){
		printf("Caught signal %d, exiting loop and waiting for writing threads to complete.\nTo force exit, hit Ctrl+C again.\n", s);
		loop = 0;
	} else {
		output_lock.try_lock();
		output_lock.unlock();
		bulk_lock.try_lock();
		bulk_lock.unlock();
		exit(3);
	}
}

FILE * check_file(const char * dir, const char *fname)
{
	FILE * fp = NULL;
	char path[256];

	// Check directory first then /usr/share/rpiboot
	if(dir)
	{
		strcpy(path, dir);
		strcat(path, "/");
		strcat(path, fname);
		fp = fopen(path, "rb");
	}

	if(fp == NULL)
	{
		strcpy(path, "/usr/share/rpiboot/");
		strcat(path, fname);
		fp = fopen(path, "rb");
	}

	return fp;
}

boot_message_t boot_message;
void *second_stage_txbuf;

int second_stage_prep(FILE *fp)
{
	int size;

	fseek(fp, 0, SEEK_END);
	boot_message.length = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	second_stage_txbuf = (uint8_t *) malloc(boot_message.length);
	if (second_stage_txbuf == NULL)
	{
		printf("Failed to allocate memory\n");
		return -1;
	}

	size = fread(second_stage_txbuf, 1, boot_message.length, fp);
	if(size != boot_message.length)
	{
		printf("Failed to read second stage\n");
		return -1;
	}

	return 0;
}

class USBDevice{
public:
	USBDevice(libusb_device_handle& device_in) : dev(&device_in), valid(true), serial(-1){
		struct libusb_config_descriptor *config;
		struct libusb_device_descriptor desc;
		struct libusb_device *usb_dev;
		int ret;

		libusb_get_active_config_descriptor(libusb_get_device(dev), &config);

		// Handle 2837 where it can start with two interfaces, the first is mass storage
		// the second is the vendor interface for programming
		if(config->bNumInterfaces == 1)
		{
			interface = 0;
			out_ep = 1;
			in_ep = 2;
		}
		else
		{
			interface = 1;
			out_ep = 3;
			in_ep = 4;
		}

		ret = libusb_claim_interface(dev, interface);
		if (ret)
		{
			libusb_close(dev);
			dev = NULL;
			printf("Failed to claim interface\n");
			valid = false;
		}

		libusb_get_device_descriptor(libusb_get_device(dev), &desc);
		serial = desc.iSerialNumber;

		// construct a string to represent the tree of port numbers of this
		// device
		usb_dev = libusb_get_device(dev);
		uint8_t port_nums[8] = {0};

		ret = libusb_get_port_numbers(usb_dev, port_nums, 8);
		if(ret == LIBUSB_ERROR_OVERFLOW){
			printf("Could not determine port tree\n");
			valid=false;
		} else {
			ostringstream os;
			// find the bus number
			os << static_cast<unsigned int>(libusb_get_bus_number(usb_dev)) << ":";

			// And the port tree
			os << static_cast<unsigned int>(port_nums[0]);
			for(int i=1; i<ret; i++){
				os << "-" << static_cast<unsigned int>(port_nums[i]);
			}

			location = os.str();
		}

		if(verbose) {
			if(valid){
				printf("Initialized device correctly\n");
			}else{
				printf("Error initializing device\n");
			}
		}

	}

	~USBDevice(){
		if(dev){
			libusb_release_interface(dev, interface);
			int rc = libusb_reset_device(dev);
			if(verbose && rc != 0) cout << "RESET RC: " << rc << endl;
			libusb_close(dev);
			dev = NULL;
		}
	}

	bool isValid() const{
		return valid;
	}

	int getSerial() const{
		return serial;
	}

	const string& getLocation() const{
		return location;
	}

	libusb_device_handle* getHandle() const{
		return dev;
	}

	int ep_write(void *buf, int len)
	{
		int a_len = 0;
		int ret =
		    libusb_control_transfer(dev, LIBUSB_REQUEST_TYPE_VENDOR, 0,
					    len & 0xffff, len >> 16, NULL, 0, 1000);

		if(ret != 0)
		{
			printf("Failed control transfer\n");
			return ret;
		}

		if(len > 0)
		{
			ret = libusb_bulk_transfer(dev, out_ep, (unsigned char *) buf, len, &a_len, 100000);
			if(verbose)
				printf("libusb_bulk_transfer returned %d\n", ret);
		}

		return a_len;
	}

	int ep_read(void *buf, int len)
	{
		int ret =
		    libusb_control_transfer(dev,
					    LIBUSB_REQUEST_TYPE_VENDOR |
					    LIBUSB_ENDPOINT_IN, 0, len & 0xffff,
					    len >> 16, (unsigned char *) buf, len, 1000);
		if(ret >= 0)
			return len;
		else
			return ret;
	}

	int second_stage_boot()
	{
		int size, retcode = 0;

		size = ep_write(&boot_message, sizeof(boot_message));
		if (size != sizeof(boot_message))
		{
			printf("Failed to write correct length, returned %d\n", size);
			return -1;
		}

		if(verbose) printf("Writing %d bytes\n", boot_message.length);
		size = ep_write(second_stage_txbuf, boot_message.length);
		if (size != boot_message.length)
		{
			printf("Failed to read correct length, returned %d\n", size);
			return -1;
		}

		sleep(1);
		size = ep_read((unsigned char *)&retcode, sizeof(retcode));

		if (size > 0 && retcode == 0)
		{
			if(verbose){
				printf("Successful read %d bytes \n", size);
			}
		}
		else
		{
			printf("Failed : 0x%x", retcode);
		}


		return retcode;

	}

	int file_server()
	{
		int going = 1;
		int allowed_err = 10;
		struct file_message {
			int command;
			char fname[256];
		} message;
		static FILE * fp = NULL;

		while(going && allowed_err > 0)
		{
			char message_name[][20] = {"GetFileSize", "ReadFile", "Done"};
			int i = ep_read(&message, sizeof(message));
			if(i < 0)
			{
				sleep(1);
				cout << "Error in ep_read" << endl;
				--allowed_err;
				continue;
			}
			if(verbose) printf("Received message %s: %s\n", message_name[message.command], message.fname);

			// Done can also just be null filename
			if(strlen(message.fname) == 0)
			{
				ep_write(NULL, 0);
				break;
			}

			switch(message.command)
			{
				case 0: // Get file size
					if(fp)
						fclose(fp);
					fp = check_file(directory, message.fname);
					if(strlen(message.fname) && fp != NULL)
					{
						int file_size;

						fseek(fp, 0, SEEK_END);
						file_size = ftell(fp);
						fseek(fp, 0, SEEK_SET);

						if(verbose) printf("File size = %d bytes\n", file_size);

						int sz = libusb_control_transfer(dev, LIBUSB_REQUEST_TYPE_VENDOR, 0,
						    file_size & 0xffff, file_size >> 16, NULL, 0, 1000);

						if(sz < 0)
							return -1;
					}
					else
					{
						ep_write(NULL, 0);
						if(verbose) printf("Cannot open file %s\n", message.fname);
						break;
					}
					break;

				case 1: // Read file
					if(fp != NULL)
					{
						int file_size;
						void *buf;

						if(verbose) printf("File read: %s\n", message.fname);

						fseek(fp, 0, SEEK_END);
						file_size = ftell(fp);
						fseek(fp, 0, SEEK_SET);

						buf = malloc(file_size);
						if(buf == NULL)
						{
							printf("Failed to allocate buffer for file %s\n", message.fname);
							return -1;
						}
						int read = fread(buf, 1, file_size, fp);
						if(read != file_size)
						{
							printf("Failed to read from input file\n");
							free(buf);
							return -1;
						}

						int sz = ep_write(buf, file_size);

						free(buf);
						fclose(fp);
						fp = NULL;

						if(sz != file_size)
						{
							printf("Failed to write complete file to USB device\n");
							return -1;
						}
					}
					else
					{
						if(verbose) printf("No file %s found\n", message.fname);
						ep_write(NULL, 0);
					}
					break;

				case 2: // Done, exit file server
					going = 0;
					break;

				default:
					printf("Unknown message\n");
					return -1;
			}
		}

		if(verbose) printf("Second stage boot server done\n");
		
		return (allowed_err > 0) ? 0 : -1;
		
	}

private:
	libusb_device_handle* dev;
	int in_ep;
	int out_ep;
	int interface;
	bool valid;
	int serial;
	string location;


};

void usage(int error)
{
	output_lock.lock();
	
	FILE * dest = error ? stderr : stdout;

	fprintf(dest, "Usage: rpiboot\n");
	fprintf(dest, "   or: rpiboot -d [directory]\n");
	fprintf(dest, "Boot a Raspberry Pi in device mode either directly into a mass storage device\n");
	fprintf(dest, "or provide a set of boot files in a directory from which to boot.  This can\n");
	fprintf(dest, "then contain a initramfs to boot through to linux kernel\n\n");
	fprintf(dest, "rpiboot                  : Boot the device into mass storage device\n");
	fprintf(dest, "rpiboot -d [directory]   : Boot the device using the boot files in 'directory'\n");
	fprintf(dest, "Further options:\n");
	fprintf(dest, "        -l               : Loop forever\n");
	fprintf(dest, "        -b               : Bulk flashing mode; flash to all devices found\n");
	fprintf(dest, "        -y               : Assume 'yes' to all confirmation.  NOTE: can be EXTREMELY\n");
	fprintf(dest, "                           DANGEROUS, use with extreme care.\n");
	fprintf(dest, "        -i [image]       : Burn the given image onto the Pi\n");
	fprintf(dest, "        -v               : Verbose\n");
	fprintf(dest, "        -h               : This help\n");

	output_lock.unlock();

	exit(error ? -1 : 0);
}

void Initialize_Devices(libusb_context ** ctx, map<string, pair<USBDevice*, unsigned char> >& device_map)
{
	struct libusb_device **devs;
	struct libusb_device *dev;
	struct libusb_device_handle *handle = NULL;
	uint32_t i = 0;


	if (libusb_get_device_list(*ctx, &devs) < 0)
		return;

	while ((dev = devs[i++]) != NULL) {
		struct libusb_device_descriptor desc;
		if(libusb_get_device_descriptor(dev, &desc) < 0){
			break;
		}

		if(verbose)
			printf("Found device %u idVendor=0x%04x idProduct=0x%04x\n", i, desc.idVendor, desc.idProduct);

		if (desc.idVendor == 0x0a5c &&
		    (desc.idProduct == 0x2763 || desc.idProduct == 0x2764) ){

			if(verbose) printf("Device located successfully\n");

			if(libusb_open(dev, &handle) >= 0){
				if(verbose) printf("Device created successfully\n");

				USBDevice* usbdev = new USBDevice(*handle);

				if(usbdev->isValid()){
					string key(usbdev->getLocation());
					// check to see if we've seen this previously

					map<string, pair<USBDevice*, unsigned char> >::const_iterator dm_itr =
							device_map.find(usbdev->getLocation());
					unsigned char n_seen = 0;

					if(dm_itr != device_map.end()){
						// this is NOT the first time seeing this device...

						// delete the old USBDevice if it exists
						if((dm_itr->second).first != 0){
							delete (dm_itr->second).first;
						}

						// add 1 to the number of times seen counter
						n_seen = (dm_itr->second).second + 1;

					}

					if(verbose) {
						cout << "Inserting device at location " << key << ", seen "
							 << static_cast<unsigned int>(n_seen) << " times" << endl;
					}

					// insert the appropriate element
					device_map[key] = std::make_pair(usbdev, n_seen);
				} else {
					// bad things happened, let's just ignore it...
					delete usbdev;
				}

			} else {
				if(verbose) printf("Failed to open the requested device\n");
			}
		}
	}

	// go through and remove any devices that are no longer present
	for(map<string, pair<USBDevice*, unsigned char> >::iterator dmit = device_map.begin(); dmit != device_map.end(); ){
		if((dmit->second).first == 0){
			// device no longer present, delete it!
			device_map.erase(dmit++);
		} else{
			++dmit;
		}
	}


	// free the device list
	libusb_free_device_list(devs, 1);

}

void get_options(int argc, char *argv[])
{
	// Skip the command name
	argv++; argc--;
	while(*argv)
	{
		if(strcmp(*argv, "-d") == 0)
		{
			argv++; argc--;
			if(argc < 1)
				usage(1);
			directory = *argv;
		}
		else if(strcmp(*argv, "-h") == 0 || strcmp(*argv, "--help") == 0)
		{
			usage(0);
		}
		else if(strcmp(*argv, "-l") == 0)
		{
			loop = 1;
		}
		else if(strcmp(*argv, "-v") == 0)
		{
			verbose = 1;
		}
		else if(strcmp(*argv, "-b") == 0)
		{
			bulk = 1;
		}
		else if(strcmp(*argv, "-y") == 0)
		{
			assumeY = 1;
		}
		else if(strcmp(*argv, "-i") == 0)
		{
			argv++; argc--;
			if(argc < 1)
				usage(1);
			image = *argv;
			
			// check to make sure that the image exists and is readable
			FILE* fp = fopen(image, "rb");
			if(fp == 0){
				fprintf(stderr, "ERROR: Could not open desired image: %s; received error: %s\n", image, strerror(errno));
				exit(2);
			}
			fclose(fp);
		}
		else
		{
			usage(1);
		}

		argv++; argc--;
	}
}

void flash_device(string image, string device){
	int BUFLEN=1024*8;
	unsigned char buf[BUFLEN];
	int nchar;
	int nwrite = 0;
	unsigned int total = 0;
	int in = open(image.c_str(), O_RDONLY);
	int out = open(device.c_str(), O_WRONLY);
	
	// wait here until we have the go-ahead in bulk mode
	bulk_lock.lock();
	bulk_lock.unlock();
	
	// wait for 1s, just in case we unlocked to make a clean exit
	sleep(1);
	
	if(in && out){
		while( nwrite != -1 && (nchar = read(in, buf, BUFLEN)) > 0){
			nwrite = write(out, buf, nchar);
			if(nwrite == -1){
				cout << "Write error: " << errno << endl;
			}
			
			total += nchar;
			//cout << "wrote " << nwrite << ", read " << total << endl;
			fsync(out);
		}
			
		close(in);
		close(out);
	
		output_lock.lock();
		cout << "Finished flashing " << device << ", wrote " << total << " bytes" << endl;
		output_lock.unlock();
	} else {
	
		if(in){
			close(in);
		}
		
		if(out){
			close(out);
		}		
	
		output_lock.lock();
		cout << "Error writing " << image << " to device " << device << endl;
		output_lock.unlock();
	}
}

void sendStage2(set<USBDevice*> devs){
	// Do the second stage boot first (note: stage 1 is in ROM)
	unsigned int nstage2 = 0;

	for(set<USBDevice*>::iterator it = devs.begin(); it!= devs.end(); it++){
		if((*it)->getSerial() == 0){
			if(verbose) printf("Sending bootcode.bin\n");
			if((*it)->second_stage_boot() != 0){
				cout << "Error sending stage 1 to " << (*it)->getLocation() << endl;
			} else {
				++nstage2;
			}
		} else {
			cout << "Not ready for stage 1 at " << (*it)->getLocation() << endl;
		}
	}

	cout << "Sent stage 1 to " << nstage2 << " units" << endl;
}

int main(int argc, char *argv[])
{
	FILE * second_stage;
	libusb_context *ctx;
	
	std::set<std::string> avail_blockd;
	std::set<boost::thread*> io_threads;
	std::set<std::string> bulk_devs;
	
	// used for join_all at end of loop
	boost::thread_group grp;

	// override the Ctrl+C handler
	struct sigaction sigIntHandler;
	
	killed = 0;

	sigIntHandler.sa_handler = my_handler;
	sigemptyset(&sigIntHandler.sa_mask);
	sigIntHandler.sa_flags = 0;

	sigaction(SIGINT, &sigIntHandler, NULL);

	get_options(argc, argv);

	// flush immediately
	setbuf(stdout, NULL);

#if defined (__CYGWIN__)
	//printf("Running under Cygwin\n");
#else
	//exit if not run as sudo
	if(geteuid() != 0)
	{
		printf("Must be run with sudo...\n");
		exit(-1);
	}
#endif

	output_lock.lock();

	// Default to standard msd directory
	if(directory == NULL)
		directory = "msd";

	second_stage = check_file(directory, "bootcode.bin");
	if(second_stage == NULL)
	{
		fprintf(stderr, "Unable to open 'bootcode.bin' from /usr/share/rpiboot or supplied directory\n");
		usage(1);
	}
	if(second_stage_prep(second_stage) != 0)
	{
		fprintf(stderr, "Failed to prepare the second stage bootcode\n");
		exit(-1);
	}


	int ret = libusb_init(&ctx);
	if (ret)
	{
		printf("Failed to initialise libUSB\n");
		exit(-1);
	}

	libusb_set_debug(ctx, verbose ? LIBUSB_LOG_LEVEL_WARNING : 0);
	
	if(bulk && loop){
		printf("WARNING: both -b and -l options provided.  Ignoring -b option\n");
		bulk=0;
	}
	
	// lock the bulk lock
	if(bulk){
		bulk_lock.lock();
	}

	vector<USBDevice*> devices;
	// mapping of physical location to USB device and number of times seen
	map<string, pair<USBDevice*, unsigned char> > device_map;
	// set of devices destined for stage 2 or 3, respectively
	set<USBDevice*> stage2_dev;
	set<USBDevice*> stage3_dev;

	bool init_one = false;

	do
	{

		if(verbose) printf("Waiting for BCM2835/6/7\n");
		output_lock.unlock();
		sleep(5);
		output_lock.lock();

		// Wait for a device to get plugged in
		do
		{
			devices.clear();

			Initialize_Devices(&ctx, device_map);

			// wait 1s before reinitializing
			if (devices.size() == 0)
			{
				output_lock.unlock();
				sleep(1);
				output_lock.lock();
			}

		}
		while (bulk == 0 && killed == 0 && (loop || (init_one == false && device_map.size() == 0)) );
		
		
		if(killed){
			break;
		}
		
		if(image != NULL){
			// Assuming that we want to burn an image, let's find all of the root partitions 
			// currently available (BEFORE loading the MSD driver)
			DIR *dir;
			struct dirent *ent;
			if ((dir = opendir ("/sys/block")) != NULL) {
				avail_blockd.clear();
				while ((ent = readdir (dir)) != NULL) {
					avail_blockd.insert(ent->d_name);
				}
				closedir (dir);
			}
		}
		
		if(device_map.size() > 0){
			cout << "Found " << device_map.size() << " units" << endl;
		}

		// Now, let's split them into who needs a stage 2 and who needs stage 3
		stage2_dev.clear();
		stage3_dev.clear();

		for(map<string, pair<USBDevice*, unsigned char> >::const_iterator it = device_map.begin(); it != device_map.end(); it++){
			if(verbose){
				cout << "Examining device at " << it->first << ", seen "
				     << static_cast<unsigned int>((it->second).second) << " times" << endl;
			}


			if((it->second).second == 0){
				// first time seen, must be stage 2
				stage2_dev.insert((it->second).first);
			} else if ((it->second).second == 1){
				stage3_dev.insert((it->second).first);
				// second time seen, SHOULD be stage 3
			} else if ((it->second).second == 2){
				cout << "Found USB too many times at " << it->first << endl;
			}
		}

		sendStage2(stage2_dev);

		unsigned int nstage3 = 0;
		// Now, the "stage 3 boot" where the devices turn into a readable block device
		for(set<USBDevice*>::iterator it = stage3_dev.begin(); it!= stage3_dev.end(); it++){
		
			cout << "Initializing location " << (*it)->getLocation() << endl;
			if((*it)->getSerial() == 0){
				cout << "WARNING: Seen USB device twice, but not in stage 3 at location " << (*it)->getLocation() << endl;
			} else {
				++nstage3;
				if(verbose) printf("Starting USB file server.\n");
				int retcode = (*it)->file_server();
				if(retcode != 0){
					cout << "Error initializing location " << (*it)->getLocation() << endl;
					continue;
				}

				init_one=true;

				// Flash the image here
				if(image != NULL){
		
					FILE* in;
					FILE* out;

					string device = "";
					string answer;

					std::set<std::string> new_blockd;
					DIR *dir;
					struct dirent *ent;
					int ntries = 20;

					while(--ntries > 0 && new_blockd.size() == 0){
						// wait for block device to come online
						sleep(1);

						if ((dir = opendir ("/sys/block")) != NULL) {
							while ((ent = readdir (dir)) != NULL) {
								if(avail_blockd.count(ent->d_name) == 0){
									//cout << "Found new block device: " << ent->d_name << endl;
									new_blockd.insert(ent->d_name);
								}
							}
							closedir (dir);
						}
					}


					if(new_blockd.size() != 1){
						if (!bulk){
							cout << "WARNING: Could not determine appropriate device.  Please enter block device: ";
							cin >> device;
							boost::algorithm::trim(device);
							while(device.size() == 0){
								cout << "Please enter a device name: ";
								cin >> device;
								boost::algorithm::trim(device);
							}
						} else {
							cout << "WARNING: Could not determine appropriate device. Error programming " << (*it)->getLocation() << endl;
						}

					} else {
						device = "/dev/" + (*new_blockd.begin());
					}

					// add all new block devices so we don't see them the next time through this loop
					avail_blockd.insert(new_blockd.begin(), new_blockd.end());

					// At this point, we have the block device...
					out = fopen(device.c_str(), "rb+");
					while(!bulk && out == 0 && device.size() > 0){
						cout << "Error: could not open " << device << " for writing.  Please enter block device (leave empty to cancel):";
						cin >> device;
						boost::algorithm::trim(device);
						if(device.size() > 0){
							out = fopen(device.c_str(), "rb+");
						}
					}
					if(out != 0){
						fclose(out);
					}
					
					if(device.size() > 0){

						if(assumeY){
							cout << "Flashing device " << device << endl;
						} else {
							cout << "Flashing device " << device << ", is this correct [yes/no]? ";
							cin >> answer;
							boost::algorithm::trim(answer);
							boost::algorithm::to_lower(answer);
							while( !(answer == "yes" || answer == "no")){
								cout << "Flashing device " << device <<", correct?  You said '" << answer << "',  Please answer 'yes' or 'no': ";
								cin >> answer;
								boost::algorithm::trim(answer);
								boost::algorithm::to_lower(answer);
							}
						}
					
						if(assumeY || answer == "yes"){
							// sleep for 2s to make all mounts appear
							if(assumeY){
								sleep(2);
							}
							// let's unmount any partitions
							ifstream mountf("/etc/mtab");
							string mountln;
							vector<string> mountdata;
							while(getline(mountf, mountln)){
								boost::algorithm::split(mountdata, mountln,boost::is_any_of("\t "),boost::token_compress_on);
								if(mountdata.size() >= 2){
									if(boost::algorithm::starts_with(mountdata[0], device)){
										cout << "Unmounting: " << mountdata[1] << endl;
										int numount = 0;
										int rc = -1;
										while(++numount < 5 && rc != 0){
											rc = umount(mountdata[1].c_str());
											int umount_err = errno;
											if(rc != 0 && umount_err != ENOENT){
												sleep(1);
												cout << "Error unmounting " << mountdata[1] << ": " << strerror(errno);
												if(numount < 4){
													cout << ", retrying";
												} else {
													cout << ", giving up";
												}
												cout << endl;
											}
										}
									}
								}
							}

							// Wait 1s to get all mounts unmounted
							sleep(1);

							// spin up a new thread that does the flashing
							in = fopen(image, "rb");
							if(in == 0){
								cout << "Could not open input image.  This should not happen." << endl;
							} else {
								// create a new thread
								io_threads.insert(new boost::thread(&flash_device, image, device));
								if(bulk){
									bulk_devs.insert(device);
								}
							}
							fclose(in);

						} else {
							cout << "Canceled writing to device " << device << endl;
						}
					}
				}
			}
		}
		
		cout << "Sent stage 2 to " << nstage3 << " units" << endl;
		// iterate through the threads to see if any are finished.  If so, clean them up
		for(std::set<boost::thread*>::iterator itr = io_threads.begin(); itr != io_threads.end(); ){
			if((*itr)->try_join_for(boost::chrono::nanoseconds(1))){
				delete *itr;
				io_threads.erase(itr++);
				cout << "Removing thread" << endl;
			} else {
				++itr;
			}
		}

		for(map<string, pair<USBDevice*, unsigned char> >::iterator it = device_map.begin(); it!= device_map.end(); it++){
			delete (it->second).first;
			(it->second).first = 0;
		}
		// NOTE: DO NOT clear the device list; we use the size in the conditional

		if(verbose){
			cout << "Still have " << device_map.size() << " enumerated" << endl;
			if(bulk){
				cout << "Stage 1: " << stage2_dev.size() << endl;
				cout << "Stage 2: " << stage3_dev.size() << endl;
			}

			cout << "LOOP conditions:" << endl;
			cout << "loop : " << static_cast<bool>(loop) << endl;
			cout << "(bulk != 0 && device_map.size() > 0) : " << (bulk != 0 && device_map.size() > 0) << endl;
			cout << "bulk && (stage2_dev.size() != 0 || stage3_dev.size() != 0 : " << (bulk && (stage2_dev.size() != 0 || stage3_dev.size() != 0)) << endl;
		}

	}
	while(loop || (bulk == 0 && device_map.size() > 0) || (bulk && (stage2_dev.size() != 0 || stage3_dev.size() != 0)));
	
	if(bulk){
		if(bulk_devs.size() > 0){
			cout << "Preparing to flash " << bulk_devs.size() << " devices.  If this is incorrect, hit Ctrl+C in the next 5 seconds";
			for(int i=0; i<5; i++){
				cout << "." << std::flush;
				sleep(1);
			}
			cout << endl;
		}		
		bulk_lock.unlock();
	}
	
	if(image != NULL && io_threads.size() > 0){
		cout << "Waiting for I/O threads to complete..." << endl;
	}
	
	output_lock.unlock();
	
	// wait for threads to complete
	for(std::set<boost::thread*>::iterator itr = io_threads.begin(); itr != io_threads.end(); itr++){
		grp.add_thread(*itr);
	}
	
	// threads now managed by grp
	io_threads.clear();
	
	// wait for threads to complete
	grp.join_all();

	if(bulk && bulk_devs.size() > 0){
		cout << "Bulk flashing complete.  Wrote " << bulk_devs.size() << " devices" << endl;
	}

	libusb_exit(ctx);

	return 0;
}

